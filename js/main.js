angular.module('firstApp', [])
.controller('main', ['$scope', function($scope) {

  $scope.greeting = 'An introduction to data binding.';

  $scope.beers = ['India Pale Ale', 'Porter', 'Stout'];

  $scope.addRow = function(){
    $scope.beers.push($scope.addBeer);
    $scope.addBeer="";
  };

  $scope.deleteRow = function(beer){
    $scope.beers.splice($scope.beers.indexOf(beer),1);
  };

}]);